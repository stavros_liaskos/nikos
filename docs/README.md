## package.json explained
https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#adding-a-css-preprocessor-sass-less-etc

## Host local production server
```bash
The project was built assuming it is hosted at http://www.stavrosliaskos.com/.
You can control this with the homepage field in your package.json.

The build folder is ready to be deployed.
You may also serve it locally with a static server:

  npm install -g pushstate-server
  pushstate-server build
  open http://localhost:9000
  ```