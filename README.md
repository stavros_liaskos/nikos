## http://lucky-barber.com

> Based on `create-react-app`  
> Read documentation at: https://github.com/facebook/create-react-app

Install only with NPM!

```
npm commands:
start: builds for dev and starts a server with hotreload
build: 
test: 
eject: DO NOT RUN THIS! This will eject the project from create-react-app!
```

### Deploy
Just running this command will deploy the app with now:
```bash
npm run deploy

```

### Tech Stack
- node ^8.11.1 (required)
