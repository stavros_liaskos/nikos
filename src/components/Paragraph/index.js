import React, {Component} from 'react';
import './index.css';
import $ from 'jquery';
import '../../assets/js/shuffle';

window.$ = $;
// todo: add comments to functions

let extendJQuery = null;
let visibilityToggle = null;
let startShuffle = null;
let lineSplitter = null;

class Paragraph extends Component {
  constructor(props) {
    super(props);

    this.state = {
      height: null
    };
  };

  extendJQuery = () => {
    $.fn.visible = function () {
      return this.css('visibility', 'visible');
    };

    $.fn.invisible = function () {
      return this.css('visibility', 'hidden');
    };

    $.fn.visibilityToggle = function () {
      return this.css('visibility', function (i, visibility) {
        return (visibility === 'visible') ? 'hidden' : 'visible';
      });
    };
  };

  visibilityToggle = (el) => {
    //return (el.style.visibility === 'visible') ? 'hidden' : 'visible';
    return el.style.visibility = 'visible';
  };

  calcHeight(node) {
    if (node && !this.state.height) {
      this.setState({
        height: node.offsetHeight
      });
    }
  }

  lineSplitter = () => {
    let pObj = $(document.getElementsByClassName('paragraph-copy-paragraph Intro'));
    // wrap all words
    pObj.each(function () {
      let obj = $(this);
      let html = obj.html().replace(/(\S+\s*)/g, "<span>$1</span>");
      obj.html(html);
    });

    let offset = 0; // keeps track of distance from top
    let spans = pObj.find("span"); // collection of elements
    let html = "";

    function getLine(index) {
      let top = 0, buffer = [];

      for (let i = 0; i < spans.length; i++) {
        if (top > index) {
          break; // quit once the line is done to improve performance
        }

        // position calculation
        let newOffset = spans[i].offsetTop;
        if (newOffset !== offset) {
          offset = newOffset;
          top++;
        }

        // store the elements in the line we want
        if (top === index) {
          buffer.push(spans[i]);
        }
      }
      // buffer now contains all spans in the X line position

      // this block is just for output purposes
      let text = "";
      for (let i = 0; i < buffer.length; i++) {
        text += buffer[i].innerHTML;
      }

      if (text.length)
        html += "<p>" + text + "</p>";

      return text.length;
    }

    let line = 1; // the line to select/highlight
    // other recalculation triggers can be added here, such as a button click
    while (getLine(line++)) {
    }
    pObj.html(html);
    //callback();
  };

  startShuffle = () => {
    let sentence = 0; // which sentence?
    if (!sentence++) {
      let userText = document.getElementsByClassName('paragraph-copy-paragraph Intro');
      let userTextParagraphsArr = $(userText).children();
      // Shuffle the contents of container
      userTextParagraphsArr.each(function (index, p) {
        setTimeout(function () {
          $(p).shuffleLetters();
          $(p).visible();
        }, index * 200);
      });
    }
  };

  componentWillMount() {
    this.extendJQuery();
  }

  componentDidMount() {
    if (this.props.category === 0) {
      this.lineSplitter();
      setTimeout(this.startShuffle, 800);
    }
    console.log('🍺');
  }

  shouldComponentUpdate(newProps, newState) {
    return !this.props.category;
  }

  componentWillUpdate(nextProps, nextState) {
    // replace multiple line p with one p for better responsiveness/design
    let userText = document.getElementsByClassName('paragraph-copy-paragraph Intro')[0];
    setTimeout(() => {
      userText.innerHTML = this.props.copy
    }, 8000);
  }

  render() {
    return (
      <div className={"paragraph-copy " + this.props.className} ref={node => this.calcHeight(node)}
           style={{height: this.state.height}}>
        <p className={"paragraph-copy-paragraph " + this.props.className}>
          {this.props.copy}
        </p>
      </div>
    );
  }
}

export default Paragraph;
