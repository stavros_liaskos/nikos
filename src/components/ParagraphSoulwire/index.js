import React, {Component} from 'react';
import './index.css';
import $ from 'jquery';
import {TextScramble} from '../../assets/js/TextScramble'
import '../../assets/js/typed';
import '../../assets/js/shuffle';
window.$ = $;

class ParagraphSoulwire extends Component {
    /**
     * animate and shuffle words in array
     */
    startTextScramble = () => {
        const phrases = [
            'Hola', 'नमस्ते', 'Hi', 'Salut', 'Konnichi wa', 'Halo', 'Kαλημέρα'
        ];

        const el = document.querySelector('.asdf');
        const fx = new TextScramble(el);

        let counter = 0;
        const next = () => {
            fx.setText(phrases[counter]).then(() => {
                setTimeout(next, 800)
            });
            counter = (counter + 1) % phrases.length
        };

        next()
    };

    extendJQuery = () => {
        $.fn.visible = function () {
            return this.css('visibility', 'visible');
        };

        $.fn.invisible = function () {
            return this.css('visibility', 'hidden');
        };

        $.fn.visibilityToggle = function () {
            return this.css('visibility', function (i, visibility) {
                return (visibility === 'visible') ? 'hidden' : 'visible';
            });
        };
    };

    startShuffle = (text) => {
        let sentence = 0; // which sentence?
        if (!sentence++) {
            let userText = document.getElementsByClassName('paragraph-copy-paragraph Info');
            let userTextParagraphsArr = $(userText).children();
            // Shuffle the contents of container
            userTextParagraphsArr.each(function (index, p) {
                setTimeout(function () {
                    $(p).shuffleLetters();
                    $(p).visible();
                }, index * 200);
//                $(p).shuffleLetters({text: text})
            });
        }
    };

    typeHeadline = (copies) => {
        let options = {
            strings: copies.headline,
            // show cursor
            showCursor: true,
            // character for cursor
            cursorChar: "",
            typeSpeed: 0,
            contentType: 'html', // or 'text'
            // call when done sentence finished
            onStringTyped: this.startShuffle(copies.copy)
        };
        $(".paragraph-headline.Info").typed(options);
    };

    lineSplitter = (callback, propsVar) => {
        var pObj = $(document.getElementsByClassName('paragraph-copy-paragraph Info'));
        // wrap all words
        pObj.each(function () {
            let obj = $(this);
            let html = obj.html().replace(/(\S+\s*)/g, "<span>$1</span>");
            //console.log(html);
            obj.html(html);
        });

        let offset = 0; // keeps track of distance from top
        let spans = pObj.find("span"); // collection of elements
        let html = "";

        function getLine(index) {
            var top = 0, buffer = [];

            for (var i = 0; i < spans.length; i++) {
                if (top > index) {
                    break; // quit once the line is done to improve performance
                }

                // position calculation
                var newOffset = spans[i].offsetTop;
                if (newOffset !== offset) {
                    offset = newOffset;
                    top++;
                }

                // store the elements in the line we want
                if (top === index) {
                    buffer.push(spans[i]);
                }
            }
            // buffer now contains all spans in the X line position

            // this block is just for output purposes
            let text = "";
            for (let i = 0; i < buffer.length; i++) {
                text += buffer[i].innerHTML;
            }

            if (text.length)
                html += "<p>" + text + "</p>";

            return text.length;
        }

        var line = 1; // the line to select/highlight
        // other recalculation triggers can be added here, such as a button click
        while (getLine(line++)) {
        }

        pObj.html(html);

        callback(propsVar);
        /*
         // throttling to handle recalculation upon resize
         var timeout;

         function throttle() {
         window.clearTimeout(timeout);
         timeout = window.setTimeout(function () {
         getLine(line);
         }, 100);
         }

         $(window).on("resize", throttle);
         */
    };

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.category === 2) {
            this.lineSplitter(this.typeHeadline, nextProps);
        }
    };

    componentDidMount = () => {
        this.extendJQuery();
        this.componentWillReceiveProps(this.props);
    };

    render() {
        return (
            <div className="paragraph-section">
                <div>
                    <h1 className={"paragraph-headline " + this.props.className}>{(this.props.category === 2) ? "" : this.props.headline}</h1>
                </div>
                <div className={"paragraph-copy " + this.props.className}>
                    <div className={"paragraph-copy-paragraph " + this.props.className}>
                        {(this.props.category === 2) ? (this.props.copy) : (<p>{this.props.copy}</p>)}
                    </div>
                </div>
            </div>
        );
    }
}

export default ParagraphSoulwire;