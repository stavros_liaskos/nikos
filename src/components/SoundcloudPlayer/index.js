import React, {Component} from 'react';
import './index.css'

class SoundcloudPlayer extends Component {

    render() {
        return (
            <div className="soundcloud-player">
                <iframe width="80%" height="20%" scrolling="no" frameBorder="no"
                        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/278386094&amp;color=16191b&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"
                />
            </div>
        );
    }

}

export default SoundcloudPlayer;