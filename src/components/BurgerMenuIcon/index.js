import React from 'react';
import './index.css'

export default React.createClass({

    render() {
        return (
            <div className={(this.props.showBurgerStatus) ? "burger-menu-wrapper active" : "burger-menu-wrapper"}
                 onClick={this.props.onBurgerClick}>
                <div className="burger-menu">
                    <span className="b-bun b-bun--top"/>
                    <span className="b-bun b-bun--mid"/>
                    <span className="b-bun b-bun--bottom"/>
                    <span className="b-bun"/>
                </div>
            </div>
        )
    }
});