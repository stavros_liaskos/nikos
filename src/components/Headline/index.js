import React, {Component} from 'react';
import './index.css';
import {init as itypedInit} from 'ityped';
// Do i need the ityped.destroy (check manual)?? -> No, destroy is for cancelling the callback function

let typeHeadline = null;

class Headline extends Component {
    typeHeadline = (node) => {
        // when unmounted, node = null and iTyped throws an error
        if (node) {
            let config = {
                strings: this.props.headline,
                //typeSpeed: 55,
                typeSpeed: 200,
                backSpeed: 100,
                startDelay: 1000,
                backDelay: 500,
                loop: true,
                saveLetters: 5,
                showCursor: true,
                cursorChar: "."
            };

            itypedInit(node, config);
        }
    };

    render() {
        return (
            <div className="paragraph-headline-wrapper">
                <h1 ref={node => this.typeHeadline(node)}/>
            </div>
        );
    }
}

export default Headline;
