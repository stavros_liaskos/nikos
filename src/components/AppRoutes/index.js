import React from 'react';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import Layout from '../../modules/Layout'
import HomePage from '../../pages/HomePage'

const routes = (
    <Router>
        <Route exact path="/" component={HomePage}/>
        <IndexRoute component={Layout}/>
    </Router>

);

export default class AppRoutes extends React.Component {
    render() {
        return (
            <Router history={hashHistory} routes={routes} onUpdate={() => window.scrollTo(0, 0)}/>
        );
    }
}
