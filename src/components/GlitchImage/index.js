import React, {Component} from 'react'
import './index.css'
import Glitch from 'image-glitch'
import {Link as ReactRouterLink} from 'react-router'
//import intro_img from '../../assets/img/intro_background.jpg'
import intro_img from '../../assets/img/under_construction.jpeg'

/**
 * cleanTime : Number Maximum duration in milliseconds that the image will remain clean while activated.
 * glitchTime : Number Maximum duration in milliseconds that the image will remain glitched while activated.
 * fuckLimit : uint Maximum amount of fucked that the image will get while activated.
 */

class GlitchImage extends Component {
    componentDidMount = () => {
        new Glitch(document.getElementById('glitch_image_id'), 2000, 80, 10);
    };

    render() {

        return (
            <div className="glitchImage">
                <ReactRouterLink to="/home" activeClassName="active" onlyActiveOnIndex={true}>
                    <img id="glitch_image_id" src={intro_img} alt="intro_glitch"/>
                </ReactRouterLink>
            </div>
        );
    }
}

export default GlitchImage;