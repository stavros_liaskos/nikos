import React from 'react';
import './index.css';

export default React.createClass({

  render() {
    return (
      <div className="counter-section">
        <div>
          <span className="satisfied-customers">Ευχαριστημένοι πελάτες: </span>
        </div>
        <div>
          <span className="animate-late" id='digit1'>6 7 8 9 0 1 2 3 4 6</span>
        </div>
        <div>
          <span className="animate" id='digit2'>6 7 8 9 5 1 2 3 4 5</span>
        </div>
        <div>
          <span className="animate-late" id='digit3'>1 2 3 4 5 6 7 8 9 0</span>
        </div>
        <div>
          <span className="animate" id='digit4'>6 7 8 9 0 1 2 3 4 6</span>
        </div>
      </div>
    );
  }
})
