import React from 'react'
import './index.css'
import {Link as ReactRouterLink} from 'react-router'

const assets = {
    'logo': require('../../assets/img/logo.png')
};

export default React.createClass({
    render() {
        return (
            <div className="logo-wrapper">
                <ReactRouterLink to="/" activeClassName="active" onlyActiveOnIndex={true}>
                    <img className="logo-image-header" src={assets.logo} alt="logo"/>
                </ReactRouterLink>
            </div>
        );
    }
})