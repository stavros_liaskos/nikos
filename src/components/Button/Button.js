import React, {Component} from 'react';
import './index.scss'; // Tell Webpack that Button.js uses these styles

class Button extends Component {

    onClick = (ev) => {
        //ev.preventDefault();
        this.props.onClick();
        console.log(this.props)
    };

    render() {
        return (
            <div className="Button">
                <a href="#" name="button" onClick={this.onClick}>Click me! {this.props.state}</a>
                <button onClick={this.onClick}>{this.props.state}</button>
            </div>
        );
    }

}

export default Button;