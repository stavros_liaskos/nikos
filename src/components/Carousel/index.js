import React from 'react';
import Carousel from 'nuka-carousel';
import './index.css';

export default React.createClass({
    mixins: [Carousel.ControllerMixin],


    render() {
        const images = this.props;

        let imagesArray = [];

        for (let key in images) {
            if (images.hasOwnProperty(key))
                imagesArray.push(images[key]);
        }
/*
        let Decorators = [{
            component: React.createClass({
                render() {
                    return (
                        <button
                            onClick={this.props.previousSlide}>
                            Previous
                        </button>
                    )
                }
            }),
            position: 'CenterLeft',
            style: {
                padding: 20
            }
        }, {
            component: React.createClass({
                render() {
                    return (
                        <button
                            onClick={this.props.nextSlide}>
                            Next
                        </button>
                    )
                }
            }),
            position: 'CenterRight',
            style: {
                padding: 20
            }
        }];
*/
        return (
            <div id="pictures-section" className="pictures">
                {/* <Carousel /!*decorators={Decorators} *!/ ref="carousel" speed={500}*/}
                <Carousel ref="carousel"
                          speed={500}
                          width="80%"
                          autoplay={true}
                          data={this.setCarouselData.bind(this, 'carousel')}>
                    {imagesArray.map(function (image, i) {
                        return <img key={i} src={image.image} alt={image.alt}/>;
                    })}
                </Carousel>

                {/*
                 {this.state.carousels.carousel ? <button type="button" onClick={this.state.carousels.carousel.goToSlide.bind(null,2)}>
                 Go to slide 3
                 </button> : null}*/}
            </div>
        );
    }
})