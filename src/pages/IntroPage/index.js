import React, {Component} from 'react'
import GlitchImage from '../../components/GlitchImage'
//import './index.css'

class IntroPage extends Component {

    render() {
        return (
            <div className="introPage">
                <GlitchImage/>
            </div>
        );
    }
}

export default IntroPage;