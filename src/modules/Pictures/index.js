import React from 'react'
import ParagraphSection from '../../modules/ParagraphSection'
import './index.css'
import ImageSlider from '../../components/ImageSlider'

export default React.createClass({
    render() {
        const {copies, images} = this.props;

        return (
            <div id="pictures-section" className="pictures">
                <ParagraphSection {...copies[2]}/>
                <ImageSlider {...images} />
            </div>
        );
    }
})