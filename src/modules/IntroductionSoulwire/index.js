import React, {Component} from 'react'
import './index.css'
import ParagraphSoulwire from '../../components/ParagraphSoulwire'

class IntroductionSoulwire extends Component {

    render() {
        const copies = this.props.copies[3];

        return (
            <div id="introductionSoulwire-section" className="introduction">
                <ParagraphSoulwire {...copies}/>
            </div>
        );
    }
}

export default IntroductionSoulwire;
