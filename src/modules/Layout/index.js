import React from 'react'
import './index.css'
//import Introduction from '../../modules/Introduction'
import Introduction from '../../modules/Introduction'
import content from '../../assets/content'

export default React.createClass({
    render() {
        return (
            <div className="content-wrapper">
                <Introduction{...content}/>
            </div>
        );
    }
})