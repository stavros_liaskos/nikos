import React from 'react'
import {Link, Events, scrollSpy, animateScroll} from 'react-scroll'
import './index.css'
import BurgerMenuIcon from '../../components/BurgerMenuIcon'
import Logo from '../../components/Logo'

export default React.createClass({
    getInitialState() {
        return {
            active: false,  // handles headers state
            showBurgerMenu: false, // handles burger menu
        }
    },

    componentDidMount() {
        // handles scrolling
        Events.scrollEvent.register('begin', function (to, element) {
//            console.log("begin", arguments);
        });

        Events.scrollEvent.register('end', function (to, element) {
//            console.log("end", arguments);
        });

        scrollSpy.update();
        window.addEventListener('scroll', this.handleScroll);
    },

    componentWillUnmount() {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
        window.removeEventListener('scroll', this.handleScroll);
    },

    handleScroll(){
        if (document.body.scrollTop > 50) {
            // update bounds in state
            this.setState({
                active: true // handles headers state
            });

        }
        else {
            this.setState({
                active: !!(this.state.showBurgerMenu)
            });
        }
    },

    toggleBurgerMenu(e) {
        e.preventDefault();

        let tmpBurger = this.state.showBurgerMenu;

        this.setState({
            showBurgerMenu: !tmpBurger,
            active: (this.state.active || !tmpBurger)
        });
    },

    render() {

        return (
            <div className={(this.state.active) ? "header-device-view active" : "header-device-view"}>
                {/* this._rootNode: declares _rootNode variable in the scope of this class */}
                {/* (node) => this._rootNode = node is the actual syntax but we use the shorter way */}
                {/* React will call the ref callback with the DOM element when the component mounts, and call it with null when it unmounts. */}
                <div className="navigation-menu">

                    <Logo/>

                    <BurgerMenuIcon showBurgerStatus={this.state.showBurgerMenu} onBurgerClick={this.toggleBurgerMenu}/>

                    <div className={(this.state.showBurgerMenu) ? "nav-menu-wrapper active" : "nav-menu-wrapper"}>

                        <div className="nav-menu-sections-wrapper">

                            <ul className={(this.state.active) ? "navigation-menu-sections activeText" : "navigation-menu-sections"}>

                                <li className="intro-head-menu">
                                    <i className="fa fa-star" aria-hidden="true">
                                        <Link activeClass="active" to="introduction-section" spy={true} smooth={true}
                                              offset={-65}
                                              duration={500}
                                              onClick={() => {
                                                  animateScroll.scrollToBottom();
                                              }}
                                              onSetActive={this.handleSetActive}> Intro
                                        </Link>
                                    </i>
                                </li>

                                <li className="music-head-menu">
                                    <i className="fa fa-music" aria-hidden="true">
                                        <Link activeClass="active" to="music-section" spy={true} smooth={true}
                                              offset={-65}
                                              duration={500}
                                              onClick={() => {
                                                  animateScroll.scrollToBottom();
                                              }}
                                              onSetActive={this.handleSetActive}> Music
                                        </Link>
                                    </i>
                                </li>

                                <li className="pics-head-menu">
                                    <i className="fa fa-picture-o" aria-hidden="true">
                                        <Link activeClass="active" to="pictures-section" spy={true} smooth={true}
                                              offset={-65}
                                              duration={500}
                                              onClick={() => {
                                                  animateScroll.scrollToBottom();
                                              }}
                                              onSetActive={this.handleSetActive}> Pics
                                        </Link>
                                    </i>
                                </li>

                                <li className="contact-head-menu">
                                    <i className="fa fa-address-book" aria-hidden="true">
                                        <a onClick={() => {
                                            animateScroll.scrollToBottom();
                                        }}> Contact</a>
                                    </i>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
})