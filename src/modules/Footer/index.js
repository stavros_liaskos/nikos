import React, {Component} from 'react';
import './index.css';

class Footer extends Component {

  render() {
    return (
      <div className="footer">
        <div className="footer-social">
          <div className="left-nav">
            <ul>
              <li>
                <a className="link" href="mailto:nicktheodosiadis@gmail.com" target="_blank">nicktheodosiadis@gmail.com</a>
              </li>
            </ul>
          </div>

          <div className="right-nav icons-view">
            <ul>
              <li>Τηλ: 6940855104</li>
              <li>Δευ-Σαβ 9<sup>00</sup>-21<sup>00</sup></li>
            </ul>
          </div>

          <div className="right-nav words-view">
            <ul>
              <li>Τηλ: 6940855104</li>
              <li>Δευ-Σαβ 9<sup>00</sup>-21<sup>00</sup></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
