import React from 'react'
import {Link, Events, scrollSpy, animateScroll} from 'react-scroll'
import './index.css'
import lodash_reduce from 'lodash/reduce'
import lodash_map from 'lodash/map'
import Logo from '../../components/Logo'

export default React.createClass({
    _nodeRefs: [],

    getInitialState() {
        return {
            active: false,  // handles headers state
            selected: null, // which header element is selected
            bounds: null    // bounds will be an array with objects. each obj = {distance,left, transform, width}
        }
    },

    calc() {
        // returns the size of an element and its position relative to the viewport. _rootNode is set using a ref
        const rootNode = this._rootNode.getBoundingClientRect();

        let first, baseWidth = rootNode.width;

        // bounds will be an array with objects. each obj = {distance,left, transform, width}
        const bounds = lodash_reduce(lodash_map(this._nodeRefs), (col, node) => {
            const {left, width} = node.getBoundingClientRect();
            if (col.length === 0) {
                first = left
            }
            /*
             console.group("CALC");
             console.count(" called calc");
             console.log(col);
             console.log(node);
             console.log("left : " + left);
             console.log("width : " + width);
             console.log("first : " + first);
             console.log("basewidth : " + baseWidth);
             console.groupEnd();
             */

            col.push({
//                left: first - rootNode.left,
                left: first - rootNode.left - baseWidth / 50,
                transform: `translateX(${-(first - left)}px) scaleX(${width / baseWidth})`,
//                width: baseWidth - 60,
                width: baseWidth - width,
                distance: -(first - left)
            });
            return col
        }, []);

        //console.log(bounds);
        this.setState({
            bounds
        })
    },

    componentDidMount() {
        // handles scrolling
        Events.scrollEvent.register('begin', function (to, element) {
//            console.log("begin", arguments);
        });

        Events.scrollEvent.register('end', function (to, element) {
//            console.log("end", arguments);
        });

        scrollSpy.update();
        window.addEventListener('scroll', this.handleScroll);


        // handles header menu underline animation
        //this.calc() // new shit
        this.timeout = setTimeout(() => {
            this.calc();
        }, 1000);

        // bind calc() on resize
        this.onResize = () => this.calc();
        window.addEventListener("resize", this.onResize);
    },

    componentWillUnmount() {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
        window.removeEventListener('scroll', this.handleScroll);

        /* new staff */
        clearTimeout(this.timeout);
        this._nodeRefs = [];
        window.removeEventListener("resize", this.onResize)
    },

    handleScroll(){
        let bounds = this.state.bounds;

        if (document.body.scrollTop > 50) {
            /**
             * apply the property "display = 'block'" to every element in bounds[]
             */
            bounds.map((bounds) => {
                Object.keys(bounds).forEach(function () {
                    bounds.display = 'block';
                });
                return bounds;
            });
            // update bounds in state
            this.setState({
                bounds,
                active: true // handles headers state
            });

        }
        else {
            bounds.map((bounds) => {
                Object.keys(bounds).forEach(function () {
                    bounds.display = 'none';
                });
                return bounds;
            });
            this.setState({
                bounds,
                active: false
            });
        }
    },

    render() {
        const {bounds} = this.state;
        const {selected} = this.state; // beta!!

        let lineStyle = {
            opacity: 0,
            left: 0
        };


        if (bounds && selected !== null) {
            lineStyle = {
                opacity: 1,
                ...bounds[selected]
                /*
                 // The above line is the same as:
                 left: bounds[selected].left,
                 width: bounds[selected].width,
                 transform: bounds[selected].transform,
                 distance: bounds[selected].distance
                 */
            };
            // console.log("HEY:  bounds: ", bounds, "state.selected: ", this.state.selected, " selected: ", selected, "left: ", lineStyle.left, " width: ", lineStyle.width, " transform: ", lineStyle.transform);
        }

        return (
            <div className={(this.state.active) ? "header-desktop-view active" : "header-desktop-view"}>
                {/* this._rootNode: declares _rootNode variable in the scope of this class */}
                {/* (node) => this._rootNode = node is the actual syntax but we use the shorter way */}
                {/* React will call the ref callback with the DOM element when the component mounts, and call it with null when it unmounts. */}
                <div ref={node => this._rootNode = node} className="navigation-menu">

                    <Logo/>

                    <div className="nav-menu-wrapper">

                        <div className="nav-menu-sections-wrapper">

                            <ul className={(this.state.active) ? "navigation-menu-sections activeText" : "navigation-menu-sections"}>

                                <li className="intro-head-menu" ref={node => this._nodeRefs[0] = node}>
                                    <Link activeClass="active" to="introduction-section" spy={true} smooth={true}
                                          offset={-65}
                                          duration={500}
                                          onClick={() => {
                                              animateScroll.scrollToBottom();
                                              let selected = 0;
                                              this.setState({selected});
                                          }}
                                          onSetActive={this.handleSetActive}>Intro
                                    </Link>
                                </li>

                                <li className="music-head-menu" ref={node => this._nodeRefs[1] = node}>
                                    <Link activeClass="active" to="music-section" spy={true} smooth={true}
                                          offset={-65}
                                          duration={500}
                                          onClick={() => {
                                              animateScroll.scrollToBottom();
                                              let selected = 1;
                                              this.setState({selected});
                                          }}
                                          onSetActive={this.handleSetActive}>Music
                                    </Link>
                                </li>

                                <li className="pics-head-menu" ref={node => this._nodeRefs[2] = node}>
                                    <Link activeClass="active" to="pictures-section" spy={true} smooth={true}
                                          offset={-65}
                                          duration={500}
                                          onClick={() => {
                                              animateScroll.scrollToBottom();
                                              let selected = 2;
                                              this.setState({selected});
                                          }}
                                          onSetActive={this.handleSetActive}>Pics
                                    </Link>
                                </li>

                                <li className="contact-head-menu" ref={node => this._nodeRefs[3] = node}>
                                    <a onClick={() => {
                                        animateScroll.scrollToBottom();
                                        let selected = 3;
                                        this.setState({selected});
                                    }}>Contact</a>
                                </li>

                            </ul>
                            {bounds && <span style={lineStyle}/>}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
})