import React from 'react'
//import './index.css'
import HeaderDesktopMenu from '../HeaderDesktopMenu'
import HeaderBurgerMenu from '../HeaderBurgerMenu'
import MediaQuery from 'react-responsive'

export default React.createClass({
    render() {
        return (
            <div className="header-container">
                <MediaQuery query='(max-width: 768px)'>
                    <HeaderBurgerMenu/>
                </MediaQuery>
                <MediaQuery query='(min-width: 768px)'>
                    <HeaderDesktopMenu/>
                </MediaQuery>
            </div>
        );
    }
})