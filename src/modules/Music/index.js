import React, {Component} from 'react';
import './index.css'

import SoundcloudPlayer from '../../components/SoundcloudPlayer'
import ParagraphSection from '../../modules/ParagraphSection'

class Music extends Component {

    render() {
        const copies = this.props.copies[1];

        return (
            <div className="music" id="music-section">
                <ParagraphSection {...copies}/>
                <SoundcloudPlayer/>
            </div>
        );
    }
}

export default Music;