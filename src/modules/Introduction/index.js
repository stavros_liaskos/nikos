import React, {Component} from 'react'
import './index.css'
import ParagraphSection from '../../modules/ParagraphSection'

class Introduction extends Component {

    render() {
        const copies = this.props.copies[0];

        return (
            <div id="introduction-section" className="introduction">
                <ParagraphSection {...copies}/>
            </div>
        );
    }
}

export default Introduction;