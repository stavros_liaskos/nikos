import React, {Component} from 'react';
import './index.css';
import Headline from '../../components/Headline';
import Paragraph from '../../components/Paragraph';

class ParagraphSection extends Component {
    render() {
        const copies = this.props;

        return (
            <div className="paragraph-container">
                <Headline {...copies} />
                <Paragraph {...copies}/>
            </div>
        );
    }
}

export default ParagraphSection;