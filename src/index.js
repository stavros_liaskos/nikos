import React from 'react';
import ReactDOM from 'react-dom';
//import App from './components/App';
import AppRoutes from './components/AppRoutes'
import './assets/styles/global.css'


/* I think reactdom is needed in order to append the first rendered component
   to somewhere (in this case the root div */
window.onload = () => {
    ReactDOM.render(<AppRoutes/>, document.getElementById('root'));
};

/*

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
*/



/*

import React from 'react';
import ReactDOM from 'react-dom';
import AppRoutes from './components/AppRoutes';

 window.onload = () => {
 ReactDOM.render(<AppRoutes/>, document.getElementById('main'));
 };
*/
