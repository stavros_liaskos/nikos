/** idx: index for each category,
 *  category: 0=(Typed.js in headline, shuffle main copy)   NOTE: headline category is redundunt! when array has one element, iTyped is disabled by default
 *  category: 1=(static content)
 *  category: 2=(Typed.js in headline, shuffle main copy, soulwire lookalike)
 */

const content = {
    copies: [
        {
            idx: 0,
            category: 0,
            className: "Intro",
            headline: ['Hello', 'नमस्ते', 'Konnichiwa','Καλημέρα', 'Halo', 'Hola', ],
            copy: "102 χρόνια εμπειρίας από τα βάθυ της Ινδίας τώρα στα μαλλιά σας."
        },
        {
            idx: 0,
            category: 1,
            className: "Music",
            headline: ['Music'],
            copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            idx: 0,
            category: 1,
            className: "Pictures",
            headline: ['Pictures'],
            copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            idx: 0,
            category: 2,
            className: "Info",
            headline: ["INFO."],
            copy: "I'm Stavros Liaskos, a Front-End Developer located in Berlin who likes to write sophisticated and creative code and loves smart hacks. I work across the full JavaScript stack and have a passion for using cutting edge technologies. Formerly developer at antoniGmbh"
        }
    ],
    images: [
        {
            idx: 0,
            category: 0,
            headline: 'Carousel1',
            copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            image: require('./img/carousel_1.jpg'),
            alt: "carousel-alt"
        },
        {
            idx: 1,
            category: 0,
            headline: 'Carousel2',
            copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            image: require('./img/carousel_2.jpg'),
            alt: "carousel-alt"
        },
        {
            idx: 2,
            category: 0,
            headline: 'Carousel3',
            copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            image: require('./img/carousel_3.jpg'),
            alt: "carousel-alt"
        },
    ]
};

export default content
